package JsonProject;

import JsonProject.RunTest_Code.APIJsonCode.RunTestSuit;
import JsonProject.base.BaseClass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@SuppressWarnings("DataProviderReturnType")
public class RunTestCase extends BaseClass {

    WebDriver driver;


    @BeforeTest
    void setup() {
        driver = launchApp();
    }

    @Test
    void RunMainTestCase() throws Throwable {
        RunTestSuit runTestSuit = new RunTestSuit();
        runTestSuit.runTestSuitCases();
    }
}
