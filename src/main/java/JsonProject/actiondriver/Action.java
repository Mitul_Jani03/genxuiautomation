package JsonProject.actiondriver;

import JsonProject.base.BaseClass;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.Set;

public class Action extends BaseClass {

    public static void NavigateURL(WebDriver driver, String navigate) throws Throwable{
        driver.get("navigate");
    }

    public static void scrollByVisibilityOfElement(WebDriver driver, WebElement ele) throws Throwable {

        JavascriptExecutor js = (JavascriptExecutor) driver;

        js.executeScript("arguments[0].scrollIntoView();", ele);

    }

    public static void click(WebDriver ldriver, WebElement locatorName) throws Throwable {

        Actions act = new Actions(ldriver);
        act.moveToElement(locatorName).click().build().perform();

    }

    public static void addnewtab(String Value) throws InterruptedException {
        String currentHandle= driver.getWindowHandle();

        ((JavascriptExecutor)driver).executeScript("window.open()");
        Set<String> handles=driver.getWindowHandles();
        for(String actual: handles)
        {

            if(!actual.equalsIgnoreCase(currentHandle))
            {
                //switching to the opened tab
                driver.switchTo().window(actual);

                //opening the URL saved.
                driver.get(Value);
                System.out.println("New Tab is Opened");
                Thread.sleep(1000);
//                driver.switchTo().window(currentHandle);
//                Thread.sleep(1000);
//                driver.switchTo().window(actual);
            }
        }
    }

    public static void backtocurrenttab() throws InterruptedException{
        String currentHandle= driver.getWindowHandle();
        driver.switchTo().window(currentHandle);
        System.out.println("Back to current Tab");
        Thread.sleep(1000);

    }

    public static boolean findElement(WebDriver ldriver, WebElement ele) throws Throwable {

        boolean flag = false;
        try {
            ele.isDisplayed();
            flag = true;

        } catch (Exception e) {
            flag = false;
        } finally {
            if (flag) {
                System.out.println("Successfully find Element");
            } else {
                System.out.println("Successfully Not found the Elemenet");
            }
        }
        return flag;
    }

    public static boolean isDisplayed(WebDriver ldriver, WebElement ele) throws Throwable {

        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isDisplayed();
            if (flag) {
                System.out.println("element displayed successfully");
            } else {
                System.out.println("element is not displayed successfully");
            }
        } else {
            System.out.println("Not Displayed");

        }
        return flag;


    }

    public static boolean isSelected(WebDriver ldriver, WebElement ele) throws Throwable {

        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isSelected();
            if (flag) {
                System.out.println("element is Selected");
            } else {
                System.out.println("element is not Selected");
            }
        } else {
            System.out.println("Not Selected");

        }
        return flag;


    }

    public static boolean isEnabled(WebDriver ldriver, WebElement ele) throws Throwable {

        boolean flag = false;
        flag = findElement(ldriver, ele);
        if (flag) {
            flag = ele.isEnabled();
            if (flag) {
                System.out.println("element is Enabled");
            } else {
                System.out.println("element is not Enabled");
            }
        } else {
            System.out.println("Not Enabled");

        }
        return flag;


    }

    public static boolean type(WebElement webElement, String text) throws Throwable {

        boolean flag = false;
        try {
            flag = webElement.isDisplayed();
            webElement.clear();
            webElement.sendKeys(text);
            flag = true;
        } catch (Exception e) {
            System.out.println("Location Not Found");
            flag = false;
        } finally {
            if (flag) {
                System.out.println("Value has entered successfully");
            } else {
                System.out.println("unable to entered value");
            }
            return flag;
        }

    }



    public static boolean selectedBySendKeys(WebElement ele, String value) throws Throwable {

        boolean flag = false;
        try {
            ele.sendKeys(value);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;

        } finally {
            if (flag) {
                System.out.println("select value from Dropdown");
            } else {
                System.out.println("Not Selected value from Dropdown");
            }

        }
    }

    public static boolean selectByIndex(WebElement element, int index) throws Throwable {

        boolean flag = false;
        try {
            Select s = new Select(element);
            s.selectByIndex(index);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;

        } finally {
            if (flag) {
                System.out.println("Option is Selected by Index");
            } else {
                System.out.println("Option is Not Selected by Index");
            }

        }
    }

    public static boolean selectByValue(WebElement element, String value) throws Throwable {

        boolean flag = false;
        try {
            Select s = new Select(element);
            s.selectByValue(value);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;

        } finally {
            if (flag) {
                System.out.println("Option is Selected by value");
            } else {
                System.out.println("Option is Not Selected by value");
            }

        }

    }

    public static boolean selectByVisibleText(String visibletext, WebElement ele) throws Throwable {

        boolean flag = false;
        try {
            Select s = new Select(ele);
            s.selectByVisibleText(visibletext);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;

        } finally {
            if (flag) {
                System.out.println("Option is Selected by visibletext");
            } else {
                System.out.println("Option is Not Selected by visibletext");
            }

        }

    }

    public static boolean mouseHoverByJavaScript(WebElement locator) throws Throwable {

        boolean flag = false;
        try {
            WebElement mo = locator;
            //   String JavaScript = "var evObj = document.createEvent('MouseEver') + "evObj.initMouseEvent(\"mouseover\",true, false, window) + "arguments[0].dispatchEvent(evObj);";
            JavascriptExecutor js = (JavascriptExecutor) driver;
            //    js.executeScript(javaScript mo);
            flag = true;
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (flag) {
                System.out.println("Mousehover is performed");
            } else {
                System.out.println("Mousehover is not performed");
            }


        }


    }

    public static boolean JSClick(WebDriver driver, WebElement ele) throws Throwable {
        boolean flag = false;
        try {
            //WebElement element = driver.findElement(locator);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", ele);
            //driver.executeAsyncScript("arguments[0].click();", element);
            flag = true;

        } catch (Exception e) {
            throw e;
        } finally {
            if (flag) {
                System.out.println("click action is performed via JavaScript");
            } else if (!flag) {

                System.out.println("click action is performed via JavaScript");

            }

        }
        return flag;
    }

    public static void mouseOverElement(WebElement element) throws Throwable {
        boolean flag = false;
        try {
            new Actions(driver).moveToElement(element).build().perform();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (flag) {
                System.out.println("Mouseover action is performed ");

            } else {
                System.out.println("Mouseover action is not performed");
            }
        }


    }

    public static void Selectpatientvialikesearch(WebElement element) throws Throwable {

        boolean flag = false;
        try {
            new Actions(driver).sendKeys(Keys.TAB);
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (flag) {
                System.out.println("Enter event is performed ");

            } else {
                System.out.println("Enter Event is not performed");
            }
        }


    }
    public static String[] getMonthYear(String monthYearVal){
        return monthYearVal.split(" ");
    }

    public static void SelectDatefromCalendar(String exDay, String exMonth, String exYear) {

        if(exMonth.equals("February") && Integer.parseInt(exDay)>29){
            System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);
            return;
        }

        if(Integer.parseInt(exDay)>31){

            System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);

        }

        String monthYearVal = driver.findElement(By.className("mat-calendar-controls")).getText();

        System.out.println(monthYearVal);



//        String month = monthYearVal.split(" ")[0].trim();
//        String year = monthYearVal.split(" ")[1].trim();

        while (!(getMonthYear(monthYearVal)[0].equals(exMonth)
                &&
                getMonthYear(monthYearVal)[1].equals(exYear)))
        {
            if (exMonth.contains("JUL") && exYear.contains("2021"))

            {
                driver.findElement(By.xpath("//button[@aria-label=\"Next month\"]")).click();

                monthYearVal = driver.findElement(By.className("mat-calendar-controls")).getText();


            } else {

                driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/mat-calendar-header/div/div/button[3]")).click();

                monthYearVal = driver.findElement(By.className("mat-calendar-controls")).getText();

            }
        }


       try {
           driver.findElement(By.xpath("//div[(contains(@class,'mat-calendar-body-cell-content')) and text()='" + exDay + "']")).click();
       }
       catch (Exception e){
           System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);
       }
        //div[contains(text(),'24')]
    }

    public static void SelectDateToCalendar(String exDay, String exMonth, String exYear) {

        if(exMonth.equals("February") && Integer.parseInt(exDay)>29){
            System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);
        }
        if(Integer.parseInt(exDay)>31){

            System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);

        }


        String monthYearVal = driver.findElement(By.className("mat-calendar-controls")).getText();

        System.out.println(monthYearVal);

        //String month = monthYearVal.split(" ")[0].trim();
        // String year = monthYearVal.split(" ")[1].trim();

        while (!(getMonthYear(monthYearVal)[0].equals(exMonth)
                &&
                getMonthYear(monthYearVal)[1].equals(exYear))) {

            driver.findElement(By.xpath("//*[@id=\"mat-datepicker-3\"]/mat-calendar-header/div/div/button[3]")).click();

            monthYearVal = driver.findElement(By.className("mat-calendar-controls")).getText();


        }
        try {
            driver.findElement(By.xpath("//div[(contains(@class,'mat-calendar-body-cell-content')) and text()='" + exDay + "']")).click();
        }
        catch (Exception e)
        {
            System.out.println("wrong Date entered: " + exMonth + " : "  + exDay);
        }

        //div[contains(text(),'24')]
    }
}












