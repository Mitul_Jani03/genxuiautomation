package JsonProject.RunTest_Code.APIJsonCode;

import JsonProject.Helper.HelperClass;
import JsonProject.actiondriver.Action;
import JsonProject.base.BaseClass;
import com.google.gson.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import static JsonProject.Helper.ReportGeneration.FullReportGenerate;

public class RunTestSuit extends BaseClass {

    public static final String PATIENT_ALLTEST_SCENARIO = "resources/testsuit/all_scenario.json";
//    private static final String EntityPath = "resources/entity/Patient/";
//    private static final String FillDataPath = "resources/apijson/patient/";

    @Test
    public void runTestSuitCases() throws Throwable {

        driver.get(prop.getProperty("url"));

        JsonArray jsonArrayALLScenario = jsonArrayResponse(PATIENT_ALLTEST_SCENARIO, "testscenario");

        for (int i = 0; i < jsonArrayALLScenario.size(); i++) {
            JsonObject jsonTestScenarioFileName = jsonArrayALLScenario.get(i).getAsJsonObject();
            String FileNameAllScenario = jsonTestScenarioFileName.get("file").getAsString();

            JsonArray jsonArrayTestScenario = jsonArrayResponse(FileNameAllScenario, "testCases");
            System.out.println("Value Test Scenario " + jsonArrayTestScenario);
            TestScenario(jsonArrayTestScenario, FileNameAllScenario);
        }

    }

    public void TestScenario(JsonArray jsonArray, String AllScenarioFileName) throws Throwable {

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonTestScenarioFileName = jsonArray.get(i).getAsJsonObject();
            String FileNameTestCases = jsonTestScenarioFileName.get("file").getAsString();
            String FileType = jsonTestScenarioFileName.get("type").getAsString();

            File scenarioFileName = new File(AllScenarioFileName);
            System.out.println("File scenarioFileName " + scenarioFileName.getName());
//            FullReportGenerate(FileNameTestCases);

            runEntityCode(FileNameTestCases, FileType);
        }
    }

    public static void runEntityCode(String FileName, String FileTypes) {
        try {

            JsonObject jsonPatiententity = jsonObjectResponse(FileName);

            String EntityFileName = EntityName(jsonPatiententity);

            JsonArray allData = jsonPatiententity.get("data").getAsJsonArray();
            JsonObject jsonMetaData = (JsonObject) jsonPatiententity.get("meta");
            System.out.println("Value FileTypes Display " + FileTypes);
            String moduleName = jsonMetaData.get("Module").getAsString();
            runFinalCode(allData, EntityFileName, moduleName, FileTypes);

        } catch (Exception e) {

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static void runFinalCode(JsonArray jsonArray, String EntityFileName, String ModuleName,
                                    String FileType) throws Throwable {

        ArrayList<Boolean> arl_Mandatory = new ArrayList<>();
        ArrayList<String> arl_KEY = new ArrayList<>();
        ArrayList<JsonObject> arl_JSONCheckData = new ArrayList<>();
        for (int p = 0; p < jsonArray.size(); p++) {
            JsonObject jsonMainLevel = jsonArray.get(p).getAsJsonObject();

            JsonArray fileTypeData = jsonMainLevel.get(FileType).getAsJsonArray();

            System.out.println("Main JSONArray " + fileTypeData);

            for (int i = 0; i < fileTypeData.size(); i++) {

                JsonObject jsonChildLevel = fileTypeData.get(i).getAsJsonObject();

                System.out.println("Current Module is working " + ModuleName);

                if (jsonChildLevel.has("filldata")) {

                    String FileNameFillData = jsonChildLevel.get("filldata").getAsString();

                    JsonObject jsonFillData = jsonObjectResponse(FileNameFillData);
                    String FillDataEntity = EntityName(jsonFillData);

                    JsonArray allData = (JsonArray) jsonFillData.get("data");
                    runFinalCode(allData, FillDataEntity, ModuleName, FileType);
                } else if (jsonChildLevel.has("check")) {
                    JsonObject jsonCheckData = (JsonObject) jsonChildLevel.get("check");
                    System.out.println("jsonCheckData" + jsonCheckData);
                    System.out.println("Validation Message Check Successfully");
                    arl_JSONCheckData.add(jsonCheckData);
//                } else if (jsonChildLevel.has("click")) {
//
//                    String str_ClickName = jsonChildLevel.get("click").getAsString();
//                    System.out.println("str_ClickName " + str_ClickName);
//                    JsonObject jsonEntityData = jsonEntityWithDataResponse(EntityFileName);
//                    if (jsonEntityData.has(str_ClickName)) {
//
//                        JsonObject jsonClickData = jsonEntityData.get(str_ClickName).getAsJsonObject();
//                        System.out.println("jsonClickData :" + jsonClickData);
//
//                        Thread.sleep(2000);
//
//                        if (jsonClickData.get("type").getAsString().equalsIgnoreCase("button")) {
//                            HelperClass.ClickEvent(jsonClickData.get("xpath").getAsString());
//                        }
//
//                        if (ModuleName.equalsIgnoreCase("Login")) {
//                            try {
//                                Thread.sleep(2000);
//                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//button[contains(text(),'Remove Remote Session')]")))) {
//                                    HelperClass.ClickEvent("//button[contains(text(),'Remove Remote Session')]");
//                                } else {
//                                    System.out.println("New Session");
//                                }
//                            } catch (Exception e) {
//                                System.out.println("Error " + e.getMessage());
//                            }
//                        } else if (ModuleName.equalsIgnoreCase("Patient Registration")) {
//                            try {
//                                Thread.sleep(2000);
//                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//b[contains(text(),'Patients Registered With Same Mobile Number :')]")))) {
//                                    HelperClass.ClickEvent("//button[contains(text(),'Ok')]");
//                                } else {
//                                    System.out.println("Patient Saved with Same Number which already exist");
//                                }
//                            } catch (Exception e) {
//                                System.out.println("Error " + e.getMessage());
//                            }
//                            try {
//                                Thread.sleep(2000);
//                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//div[contains(text(),'You are not allowed to perform this operation.')]")))) {
//                                    System.out.println("You are not allowed to perform this operation.");
//                                    Thread.sleep(2000);
//                                } else {
//                                    System.out.println("Record Saved Successfully.");
//                                }
//                            } catch (Exception e) {
//                                System.out.println("Error " + e.getMessage());
//                            }
//                            try {
//                                Thread.sleep(2000);
//                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//div[contains(text(),'Your record has been saved.')]")))) {
//                                    System.out.println("Your record has been saved.");
//                                    Thread.sleep(2000);
//                                } else {
//                                    System.out.println("Record Saved Successfully.");
//                                }
//                            } catch (Exception e) {
//                                System.out.println("Error " + e.getMessage());
//                            }
//                            try {
//                                Thread.sleep(2000);
//                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//div[contains(text(),'Your record has been updated !')]")))) {
//                                    System.out.println("Your record has been updated !");
//                                    Thread.sleep(2000);
//                                } else {
//                                    System.out.println("Record Updated Successfully.");
//                                }
//                            } catch (Exception e) {
//                                System.out.println("Error " + e.getMessage());
//                            }
//                        }
//                    }
                } else if (jsonChildLevel.has("NavigateByURL")) {
//                driver.get("https://demo.softclinicgenx.com:9091/patient/patientregistration/create");

                    String str_NavigationURL = jsonChildLevel.get("NavigateByURL").getAsString();
                    driver.get(str_NavigationURL);
                    Thread.sleep(2000);
                } else if (jsonChildLevel.has("NavigateByMenu")) {
                    JsonArray jsonNavigationURL = jsonChildLevel.get("NavigateByMenu").getAsJsonArray();
                    for (int k = 0; k < jsonNavigationURL.size(); k++) {
                        HelperClass.ClickEvent(jsonNavigationURL.get(k).getAsString());
                        Thread.sleep(2000);
                    }
                } else if (jsonChildLevel.has("complete")) {
                    String str_Complete = jsonChildLevel.get("complete").getAsString();
                    System.out.println("str_Complete " + str_Complete);
                    break;
                } else {

                    try {
                        JsonObject jsonPatiententity = jsonEntityResponse(EntityFileName);
                        JsonObject jsonAllData = jsonPatiententity.get("data").getAsJsonObject();

                        System.out.println("jsonAllData " + jsonAllData);

                        for (Map.Entry<String, JsonElement> key : jsonAllData.getAsJsonObject().entrySet()) {
                            //based on you key types
                            String keyStr = key.getKey();
                            arl_KEY.add(keyStr);
                            JsonObject keyvalue = jsonAllData.get(keyStr).getAsJsonObject();

                            JsonObject jsonMainData = keyvalue.getAsJsonObject();
                            JsonArray jsonArrayTCPath = jsonMainData.get("tcpath").getAsJsonArray();
                            System.out.println("jsonArrayTCPath " + jsonArrayTCPath);
                            arl_Mandatory.add(jsonMainData.get("mandatory").getAsBoolean());

                            if (jsonArrayTCPath.size() > 1) {

                                String[] tcPathData = toStringArray(jsonArrayTCPath);
                                String str_TCPathValue = getAllKeyValueHere(new Gson().toJson(jsonChildLevel),
                                        tcPathData);
                                System.out.println("str_TCPathValue " + str_TCPathValue);

                                if (jsonMainData.get("type").getAsString().equalsIgnoreCase("textbox")) {
//                                    driver.findElements(By.xpath("xpath")).clear();
//                                    Thread.sleep(500);
                                    HelperClass.EnterTextValue(jsonMainData.get("xpath").getAsString(), str_TCPathValue);
                                } else if (jsonMainData.get("type").getAsString().equalsIgnoreCase("Searchwithdownarrow")) {
                                    Thread.sleep(1000);
                                    HelperClass.Searchwithdownarrow(jsonMainData.get("xpath").getAsString(), str_TCPathValue);
                                } else if (jsonMainData.get("type").getAsString().equalsIgnoreCase("serachtext")) {
                                    Thread.sleep(1000);
                                    HelperClass.SearchTextValue(jsonMainData.get("xpath").getAsString(), str_TCPathValue);
                                    Thread.sleep(1000);
                                } else if (jsonMainData.get("type").getAsString().equalsIgnoreCase("isdisplayed")) {
                                    Thread.sleep(1000);
                                    HelperClass.isdisplayed(jsonMainData.get("xpath").getAsString());
                                } else if (jsonMainData.get("type").getAsString().equalsIgnoreCase("dropdown")) {
                                    Thread.sleep(1000);
                                    HelperClass.DropdownSelectValue(jsonMainData.get("xpath").getAsString(), str_TCPathValue);
                                }
                            } else {
                                if (jsonMainData.get("type").getAsString().equalsIgnoreCase("button")) {
                                    HelperClass.ClickEvent(jsonMainData.get("xpath").getAsString());
                                }if (ModuleName.equalsIgnoreCase("Login")) {
                            try {
                                Thread.sleep(2000);
                                if (Action.isDisplayed(driver, driver.findElement(By.xpath("//button[contains(text(),'Remove Remote Session')]")))) {
                                    HelperClass.ClickEvent("//button[contains(text(),'Remove Remote Session')]");
                                } else {
                                    System.out.println("New Session");
                                }
                            } catch (Exception e) {
                                System.out.println("Error " + e.getMessage());
                            }
                        }
                                Thread.sleep(1000);

                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Error Else " + e.getMessage());
                    }
                }
            }
        }

        if (arl_Mandatory.size() > 0) {
            CheckAllValidation(arl_Mandatory, arl_JSONCheckData, arl_KEY);
        }

    }

    private static JsonObject jsonObjectResponse(String EntityFileName) throws IOException, ParseException {

        String FileNameEntity = EntityFileName;
        JsonParser jsonParser = new JsonParser();
        Object objectPatientEntity = jsonParser.parse(new FileReader(FileNameEntity));
        JsonObject jsonPatiententity = (JsonObject) objectPatientEntity;
        return jsonPatiententity;
    }

    private static JsonObject jsonEntityResponse(String EntityFileName) throws IOException, ParseException {

        String FileNameEntity = EntityFileName;
        JsonParser jsonParser = new JsonParser();
        Object objectPatientEntity = jsonParser.parse(new FileReader(FileNameEntity));
        JsonObject jsonPatiententity = (JsonObject) objectPatientEntity;
        return jsonPatiententity;
    }

    private static JsonObject jsonEntityWithDataResponse(String EntityFileName) throws IOException, ParseException {

        String FileNameEntity = EntityFileName;
        JsonParser jsonParser = new JsonParser();
        Object objectPatientEntity = jsonParser.parse(new FileReader(FileNameEntity));
        JsonObject jsonPatiententity = (JsonObject) objectPatientEntity;
        JsonObject jsonAllData = jsonPatiententity.get("data").getAsJsonObject();
        return jsonAllData;
    }

    private JsonArray jsonArrayResponse(String FileName, String keyValue) throws IOException, ParseException {
        //JSON parser object to parse read file
        JsonParser jsonScenario = new JsonParser();
        Object objectScenario = jsonScenario.parse(new FileReader(FileName));

        //convert Object to JSONObject
        JsonObject jsonTestScenario = (JsonObject) objectScenario;
        JsonArray jsonArrayTestScenario = (JsonArray) jsonTestScenario.get(keyValue);
        return jsonArrayTestScenario;
    }

    public static String[] toStringArray(JsonArray array) {
        if (array == null)
            return null;

        String[] arr = new String[array.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = array.get(i).getAsString();
        }
        return arr;
    }

    public static String getAllKeyValueHere(String jsonData, String[] keys) throws ParseException {
        String jsonAll = "";
//        String[] keyMain = keys.split("\\.");
        JSONParser parser = new JSONParser();
        JSONObject js1 = (JSONObject) parser.parse(jsonData);

        String[] keyMain = keys;
        for (String keym : keyMain) {
            Iterator iterator = js1.keySet().iterator();
            String key = null;
            while (iterator.hasNext()) {
                key = (String) iterator.next();

                if (js1.get(key) != null) {
                    if ((key.equals(keym))) {
                        Object intervention = js1.get(key);

                        if (intervention instanceof JSONArray) {
                            // It's an array
//                            interventionJsonArray = (JSONArray)intervention;
                            JSONArray jArray = (JSONArray) js1.get(key);
                            for (int i = 0; i < jArray.size(); i++) {
                                js1 = (JSONObject) jArray.get(i);
//                                System.out.println(" Value " + js1);
//                                jsonAll = (String) js1.get(key);
//                                System.out.println(" Value of Array " + jsonAll);
                            }
                        } else if (intervention instanceof JSONObject) {
                            // It's an object
//                            interventionObject = (JSONObject)intervention;
                            js1 = (JSONObject) js1.get(key);
                        } else {
                            // It's something else, like a string or number
                            jsonAll = (String) js1.get(key);
                        }

                        break;
                    }
                }
//                if (js1.get(key) != null) {
//                    JSONArray jArray = (JSONArray) js1.get(key);
//                    JSONObject j;
//                    for (int i = 0; i < jArray.length(); i++) {
//                        js1 = jArray.getJSONObject(i);
//                        break;
//                    }
//                }
            }
        }
        return jsonAll;
    }

    private static String EntityName(JsonObject jsonPatient) {
        String EntityName = "";
        String EntityPath = "";
        String FinalName = "";
        JsonObject meta = jsonPatient.get("meta").getAsJsonObject();
        EntityName = meta.get("entity").getAsString();
        EntityPath = meta.get("entityPath").getAsString();
        FinalName = EntityPath + EntityName;
        return FinalName;
    }

    private static void CheckAllValidation(ArrayList<Boolean> arl_Mandatory,
                                           ArrayList<JsonObject> jsonAllCheckResponse,
                                           ArrayList<String> arl_KeyValue) {

        for (int i = 0; i < arl_Mandatory.size(); i++) {

            for (int k = 0; k < jsonAllCheckResponse.size(); k++) {

                if (arl_Mandatory.get(i)) {
                    JsonObject jsonAll = (JsonObject) jsonAllCheckResponse.get(k);

                    String IDValue = jsonAll.get("id").getAsString();
                    String pathofWidgetType = jsonAll.get("idtype").getAsString();
                    String pathIDValue = jsonAll.get("idvalue").getAsString();
                    String componentValue = jsonAll.get("componentType").getAsString();
                    String validationMSG = jsonAll.get("value").getAsString();

                    if (arl_KeyValue.get(i).equalsIgnoreCase(IDValue)) {
                        HelperClass.HelperValidationCheck(pathofWidgetType, pathIDValue, componentValue, validationMSG);
                    }
                }
            }

//                for (int j = 0; j < jsonAllCheckResponse.size(); j++) {
//
//                    JsonObject jsonAll = (JsonObject) jsonAllCheckResponse.get(j);
//
//                    String pathofWidgetType = jsonAll.get("idtype").getAsString();
//                    String pathIDValue = jsonAll.get("idvalue").getAsString();
//                    String componentValue = jsonAll.get("componentType").getAsString();
//                    String validationMSG = jsonAll.get("value").getAsString();
//
//                    HelperClass.HelperValidationCheck(pathofWidgetType, pathIDValue, componentValue, validationMSG);
//
//                }
        }
    }

    private static void ValidationCheck(ArrayList<Boolean> arl_Mandatory, JsonObject jsonAllCheckResponse) {

        JsonObject jsonAll = (JsonObject) jsonAllCheckResponse;

        for (int i = 0; i < arl_Mandatory.size(); i++) {
            if (arl_Mandatory.get(i)) {

                String pathofWidgetType = jsonAll.get("idtype").getAsString();
                String pathIDValue = jsonAll.get("idvalue").getAsString();
                String componentValue = jsonAll.get("componentType").getAsString();
                String validationMSG = jsonAll.get("value").getAsString();

                HelperClass.HelperValidationCheck(pathofWidgetType, pathIDValue, componentValue, validationMSG);
            } else {

            }
        }
    }

//    Working code
//    private void runNewCode(String FileName) throws Throwable {
//
//        System.out.println(" First Start Test Scenario ");
//
//        try {
//
//            //JSON parser object to parse read file
//            JSONParser jsonScenario = new JSONParser();
//            Object objectScenario = jsonScenario.parse(new FileReader(FileName));
//
//            //convert Object to JSONObject
//            JSONObject jsonTestScenario = (JSONObject) objectScenario;
//            JSONArray jsonArrayTestScenario = (JSONArray) jsonTestScenario.get("testCases");
//            System.out.println("Scenario Size " + jsonArrayTestScenario.size());
//
//            for (int i = 0; i < jsonArrayTestScenario.size(); i++) {
//
//                JSONObject jsonTestScenarioFileName = (JSONObject) jsonArrayTestScenario.get(i);
//                String FileNameTestCases = (String) jsonTestScenarioFileName.get("file");
//
//                //JSON parser object to parse read file
//                JSONParser jsonParser = new JSONParser();
//                Object objectTestCase = jsonParser.parse(new FileReader(FileNameTestCases));
//
//                //convert Object to JSONObject
//                JSONObject jsonTCPatient = (JSONObject) objectTestCase;
//                String EntityFileName = EntityName(jsonTCPatient);
//
//                JSONArray allData = (JSONArray) jsonTCPatient.get("data");
//                JSONObject jsonPatData = (JSONObject) allData.get(0);
//                System.out.println("Value of Data " + jsonPatData);
//
//                System.out.println("File OLD " + EntityFileName);
//
//                String FileNameEntity = "resources/entity/Patient/" + EntityFileName;
//
//                Object objectPatientEntity = jsonParser.parse(new FileReader(FileNameEntity));
//
//                JSONObject jsonPatiententity = (JSONObject) objectPatientEntity;
//
//                //based on you key types
//                JSONObject jsonAllData = (JSONObject) jsonPatiententity.get("data");
//
//                for (Object key : jsonAllData.keySet()) {
//                    //based on you key types
//                    String keyStr = (String) key;
//                    Object keyvalue = jsonAllData.get(keyStr);
//
//                    JSONObject jsonMainData = (JSONObject) keyvalue;
//                    JSONArray jsonArrayTCPath = (JSONArray) jsonMainData.get("tcpath");
//
//                    if (jsonArrayTCPath.size() > 1) {
//
//                        String[] tcPathData = toStringArray(jsonArrayTCPath);
//
//                        String str_TCPathValue = getAllKeyValue(jsonPatData,
//                                tcPathData);
//
//                        if (jsonMainData.get("type").toString().equalsIgnoreCase("textbox")) {
//                            Action.type(driver.findElement(By.xpath(jsonMainData.get("xpath").toString())),
//                                    str_TCPathValue);
//                        }
//
//                    } else {
//                        Thread.sleep(2000);
//                        if (jsonMainData.get("type").toString().equalsIgnoreCase("button")) {
//                            Action.click(driver, driver.findElement(By.xpath(jsonMainData.get("xpath").toString())));
//                        }
//                    }
//                }
//
//            }
//
//        } catch (Exception e) {
//            System.out.println("All Error " + e);
//        }
//    }

}
