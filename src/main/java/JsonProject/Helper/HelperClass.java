package JsonProject.Helper;

import JsonProject.actiondriver.Action;
import groovy.json.internal.Value;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.List;

import static JsonProject.base.BaseClass.driver;

public class HelperClass {

    public static void SearchTextValue(String xPath, String Value) throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        Action.type(driver.findElement(By.xpath(xPath)), Value);
        Thread.sleep(2000);
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@role=\"option\"]")));
        driver.findElement(By.xpath(xPath)).sendKeys(Keys.ENTER);
        Thread.sleep(1000);
    }

    public static void scrollByVisibilityOfElement(String xPath, String Value) throws Throwable {
        Action.scrollByVisibilityOfElement(driver, driver.findElement(By.xpath(xPath)));
    }

    public static void Searchwithdownarrow(String xPath, String Value) throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        Action.type(driver.findElement(By.xpath(xPath)), Value);
        Thread.sleep(1500);
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        driver.findElement(By.xpath(xPath)).sendKeys(Keys.ARROW_DOWN);
        Thread.sleep(1500);
        WebDriverWait wait3 = new WebDriverWait(driver,30);
        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        driver.findElement(By.xpath(xPath)).sendKeys(Keys.ENTER);
        Thread.sleep(1500);
    }

    public static void EnterTextValue(String xPath, String Value) throws Throwable {
//        driver.findElement(By.xpath(xPath)).clear();
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        Action.type(driver.findElement(By.xpath(xPath)), Value);
        Thread.sleep(1000);
    }

    public static void ClickEvent(String xPath) throws Throwable {
        Thread.sleep(1000);
        Action.scrollByVisibilityOfElement(driver, driver.findElement(By.xpath(xPath)));
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        Action.click(driver, driver.findElement(By.xpath(xPath)));
        Thread.sleep(1500);
    }

    public static void isdisplayed(String xPath) throws Throwable {
        Thread.sleep(1000);
        Action.isDisplayed(driver, driver.findElement(By.xpath(xPath)));
        Thread.sleep(1500);
    }

    public static void DropdownSelectValue(String xPath, String Value) throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        Action.click(driver, driver.findElement(By.xpath(xPath)));
        Thread.sleep(1000);
        Action.click(driver, driver.findElement(By.xpath("//span[contains(text(),'" + Value + "')]")));
        Thread.sleep(1000);
    }

    public static void selectcheckbox(String xpath, String Value) throws Throwable {
        List<WebElement> AllCheckboxes =  driver.findElements(By.xpath("//input[@type='radio']"));
        int size = AllCheckboxes.size();
        /*Action.click(driver, driver.findElement(By.xpath(xpath)));
        Action.click(driver, driver.findElement(By.xpath("//span[contains(text(),'" + Value + "')]")));*/
    }

    public static void addnewtab(String Value) throws InterruptedException {
//        Action.addnewtab();
//        Actions action = new Actions(driver); action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform(); //opening the URL saved.
//        driver.get(Value);
//        Action.addnewtab(driver.navigate().to());
    }
    public static String HelperValidationCheck(String idType, String WidgetXPath,
                                               String ComponentType, String ValidationMSG) {
        String Pass_Failed = "";
        String web_actual_msg = "";

        try {
            if (ComponentType.equalsIgnoreCase("textbox")) {
                if (idType.equalsIgnoreCase("xpath")) {
                    web_actual_msg = driver.findElement(By.xpath(WidgetXPath)).getText();
                } else {

                }
            }

            if (web_actual_msg.contains(ValidationMSG)) {
                System.out.println("Test Case Passed");
                Pass_Failed = "Pass";
                Constant.countPass++;
            } else if (web_actual_msg.equalsIgnoreCase("")) {
                Pass_Failed = "";
            } else {
                System.out.println("Test Case Failed");
                Pass_Failed = "Failed";
                Constant.countFail++;
            }
        } catch (NoSuchElementException nsee) {
            System.out.println("There is no such element on that ID");
            Pass_Failed = "No Element";
            Constant.countFail++;
        }

        return Pass_Failed;
    }
}