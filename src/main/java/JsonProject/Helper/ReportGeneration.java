package JsonProject.Helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.NameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import static JsonProject.Helper.Constant.*;
import static JsonProject.RunTest_Code.APIJsonCode.RunTestSuit.PATIENT_ALLTEST_SCENARIO;
import static JsonProject.RunTest_Code.APIJsonCode.RunTestSuit.runEntityCode;

public class ReportGeneration {

    static Set<File> scenariosFileSet = new TreeSet<>();
    static int count = 0;

    public static void FullReportGenerate(String FileNameAllScenario) throws IOException, ParseException {

        File[] testScenarioDir = new File(System.getProperty("user.dir"), ReportGeneratePath).listFiles();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat dirformat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = new Date();
        String DirectoryPath = "C:\\APIAutomationSummaryReport_New";
        String foldername = DirectoryPath + "\\" + dirformat.format(date);
        File FileDir = new File(foldername);
        try {
            FileDir.mkdir();

        } catch (SecurityException e) {

        }

        //Prepare Test Scenario List

        for (int i = 0; i < testScenarioDir.length; i++) {
            File directory = testScenarioDir[i];
            if (directory.isDirectory()) {
                File[] files = directory.listFiles();
                for (int j = 0; j < files.length; j++) {
                    jsonArrayResponse(files[j].toString(), "testCases", FileNameAllScenario);
                }
            }
        }

        System.out.println("\n\nFinal File List: " + scenariosFileSet);
        StopWatch watch = new StopWatch();


        //Execute Test Scenario List
        Iterator<File> files = scenariosFileSet.iterator();

        //String URL = new RestAssured().DEFAULT_URI;
        //String Username = new RefreshTokenScheduler().getRefreshToken().Username;
//        String URL = RestAssured.baseURI;
        List<NameValuePair> form = new ArrayList<>();
//        String Username = new RefreshTokenScheduler().UserName;
//        String Version = new RefreshTokenScheduler().verSion;
        try {
            //java.util.Date date= new java.util.Date();
            //Timestamp  a=new Timestamp(date.getTime());

            //String temp=a.toString();
            //System.out.println(temp);
            //File f1=new File(temp);
            //System.out.println(f1.mkdir());

            String Checkfilenamepath = FileDir.getPath();
            FileWriter fw2 = new FileWriter(Checkfilenamepath + "\\Testcase Wise Summary Report.htm");

            fw2.write("<h3>Testcase Wise Summary Report</h3>");
            fw2.write("<html><head>\n" +
                    "  <title>Bootstrap Example</title>\n" +
                    "  <meta charset=\"utf-8\">\n" +
                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
                    "</head><body>");
            fw2.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\"><th>Sr.No</th><th>File Name</th><th>Pass</th><th>Fail</th><th>Module Name</th></tr>");
            while (files.hasNext()) {

                ++count;
                File file = files.next();
                File fAllScenario = new File(FileNameAllScenario);

                //watch.start();
                // call to the methods you want to benchmark


                System.out.println("\n\nExecuting Test Case: " + file + "\n");
                System.out.println("\n\nFile Name: " + file.getName() + "\n");

                System.out.println("\n\nFile Name: " + file.getParent().substring(file.getParent().lastIndexOf('\\') + 1) + "\n");

                String fName = file.getName().toString();
                String mName = file.getParent().substring(file.getParent().lastIndexOf('\\') + 1);

                int oldtotalPass = countPass;
                int oldtotalFail = countFail;
                runEntityCode(file.toString(), "");
                int totalPass = countPass;
                int totalFail = countFail;
                if (oldtotalPass < totalPass) {

                    totalPass = 1;
                    totalFail = 0;

                } else {
                    totalPass = 0;
                    totalFail = 1;
                    int Length = fName.length() - 5;

                    String NewFileName = fName.substring(0, Length);

                    int Usindex = fName.indexOf("_");
                    String Id = NewFileName;
                    //String Md=NewModuleName;
                    //String Module=NewModuleName;
                    if (Usindex == -1) {

                    } else {
                        Id = fName.substring(0, Usindex);
                        //Md=mName.toString();
                    }

                    System.out.println(formatter.format(date));
                    System.out.println(LocalDateTime.now());
                    FileWriter fw3 = new FileWriter(FileDir.getPath() + "\\" + NewFileName + ".html");
                    //FileWriter fw3=new FileWriter(FileDir.getPath()+"\\"+NewModuleName+"");
                    fw3.write(" <h3>Fail Testcase Summary Report</h3>");

                    fw3.write("<html><head>\n" +
                            "  <title>Bootstrap Example</title>\n" +
                            "  <meta charset=\"utf-8\">\n" +
                            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                            "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                            "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                            "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +

                            "</head><body>");

                    fw3.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\" ><th>TestCase_ID</th><th>TestCase_Name</th><th>Date</th></tr>" +
                            "<tr><td>" + Id + "</td><td>" + NewFileName + "</td><td>" + formatter.format(date) + "</td></tr>" +
                            "<tr><th colspan='2'>Expected Result:</th><td>" + 5 + "</td></tr>" +
                            "<tr><th colspan='2'>Actual Result:</th><td>" + 4 + "</td></tr>" +
                            "</table>");
                    fw3.write("</body></html>");
                    fw3.close();

                }

                String NewFileName = fName.substring(0, fName.length() - 5);
                String Path = FileDir.getPath() + "\\" + NewFileName + ".html";
                //String=FileDir.getParent()+"\\"+mName+"";
                String replaceString = Path.replace('\\', '/');
                fw2.write("<tr><td>" + count + "</td><td><a  href=" + replaceString + " target=\"_blank\">" + fName + "</td><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + mName + "</td></tr>");
            }
            fw2.write("<tr><td>" + "Grand Total:" + "</td><td>" + "" + "</td><td>" + countPass + "</td><td>" + countFail + "</td></tr>");
            fw2.write("</table></body></html>");
            fw2.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
        System.out.println("Success File...");

        System.out.println("Total Number of TestCases Executed : " + count);
        //watch.stop();
        //long result = watch.getTime();
        //System.out.println("Total Execution Time : " + result+" ");


//        try {
//            int totalPass = new TestAutomation().countPass;
//            int totalFail = new TestAutomation().countFail;
//
//            FileWriter fw = new FileWriter(FileDir.getPath() + "\\Test Summary Report.htm");
//            fw.write("<html><head>\n" +
//                    "  <title>Bootstrap Example</title>\n" +
//
//                    "  <meta charset=\"utf-8\">\n" +
//                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
//                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
//                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
//                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
//
//                    "</head><body>");
//            fw.write("<h3>Test Summary Report</h3>");
//            fw.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3 \"><th>Pass </th><th>Fail </th><th>Total </th><th>Date & Time </th><td>URL</td><td>Username</td><td>Version</td></tr><tr><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + count + "</td><td>" + formatter.format(date) + "</td><td>" + RestAssured.baseURI + "</td><td>" + Username + "</td><td>" + Version + "</td></tr></table>");
//            fw.write("</body></html>");
//            fw.close();
//        } catch (Exception e) {
//            System.out.println(e);
//        }
        System.out.println("Success...");

    }

    public static void FullReportGenerate_Working(String FileNameAllScenario) throws IOException, ParseException {

        File[] testScenarioDir = new File(System.getProperty("user.dir"), ReportGeneratePath).listFiles();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat dirformat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = new Date();
        String DirectoryPath = "C:\\APIAutomationSummaryReport_New";
        String foldername = DirectoryPath + "\\" + dirformat.format(date);
        File FileDir = new File(foldername);
        try {
            FileDir.mkdir();

        } catch (SecurityException e) {

        }

        //Prepare Test Scenario List

        for (int i = 0; i < testScenarioDir.length; i++) {
            File directory = testScenarioDir[i];
            if (directory.isDirectory()) {
                File[] files = directory.listFiles();
                for (int j = 0; j < files.length; j++) {
//                    jsonArrayResponse(files[j].toString(), "testCases");
                }
            }
        }

        System.out.println("\n\nFinal File List: " + scenariosFileSet);
        StopWatch watch = new StopWatch();


        //Execute Test Scenario List
        Iterator<File> files = scenariosFileSet.iterator();

        //String URL = new RestAssured().DEFAULT_URI;
        //String Username = new RefreshTokenScheduler().getRefreshToken().Username;
//        String URL = RestAssured.baseURI;
        List<NameValuePair> form = new ArrayList<>();
//        String Username = new RefreshTokenScheduler().UserName;
//        String Version = new RefreshTokenScheduler().verSion;
        try {
            //java.util.Date date= new java.util.Date();
            //Timestamp  a=new Timestamp(date.getTime());

            //String temp=a.toString();
            //System.out.println(temp);
            //File f1=new File(temp);
            //System.out.println(f1.mkdir());

            String Checkfilenamepath = FileDir.getPath();
            FileWriter fw2 = new FileWriter(Checkfilenamepath + "\\Testcase Wise Summary Report.htm");

            fw2.write("<h3>Testcase Wise Summary Report</h3>");
            fw2.write("<html><head>\n" +
                    "  <title>Bootstrap Example</title>\n" +
                    "  <meta charset=\"utf-8\">\n" +
                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
                    "</head><body>");
            fw2.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\"><th>Sr.No</th><th>File Name</th><th>Pass</th><th>Fail</th><th>Module Name</th></tr>");
            while (files.hasNext()) {

                File file = files.next();
                File fAllScenario = new File(FileNameAllScenario);

                //watch.start();
                // call to the methods you want to benchmark


                System.out.println("\n\nExecuting Test Case: " + file + "\n");
                System.out.println("\n\nFile Name: " + file.getName() + "\n");

                System.out.println("\n\nFile Name: " + file.getParent().substring(file.getParent().lastIndexOf('\\') + 1) + "\n");

                String fName = file.getName().toString();
                String mName = file.getParent().substring(file.getParent().lastIndexOf('\\') + 1);

                int oldtotalPass = countPass;
                int oldtotalFail = countFail;
                runEntityCode(file.toString(), "");
                int totalPass = countPass;
                int totalFail = countFail;
                if (oldtotalPass < totalPass) {

                    totalPass = 1;
                    totalFail = 0;

                } else {
                    totalPass = 0;
                    totalFail = 1;
                    int Length = fName.length() - 5;

                    String NewFileName = fName.substring(0, Length);

                    int Usindex = fName.indexOf("_");
                    String Id = NewFileName;
                    //String Md=NewModuleName;
                    //String Module=NewModuleName;
                    if (Usindex == -1) {

                    } else {
                        Id = fName.substring(0, Usindex);
                        //Md=mName.toString();
                    }

                    System.out.println(formatter.format(date));
                    System.out.println(LocalDateTime.now());
                    FileWriter fw3 = new FileWriter(FileDir.getPath() + "\\" + NewFileName + ".html");
                    //FileWriter fw3=new FileWriter(FileDir.getPath()+"\\"+NewModuleName+"");
                    fw3.write(" <h3>Fail Testcase Summary Report</h3>");

                    fw3.write("<html><head>\n" +
                            "  <title>Bootstrap Example</title>\n" +
                            "  <meta charset=\"utf-8\">\n" +
                            "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                            "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                            "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                            "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +

                            "</head><body>");

                    fw3.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\" ><th>TestCase_ID</th><th>TestCase_Name</th><th>Date</th></tr>" +
                            "<tr><td>" + Id + "</td><td>" + NewFileName + "</td><td>" + formatter.format(date) + "</td></tr>" +
                            "<tr><th colspan='2'>Expected Result:</th><td>" + 5 + "</td></tr>" +
                            "<tr><th colspan='2'>Actual Result:</th><td>" + 4 + "</td></tr>" +
                            "</table>");
                    fw3.write("</body></html>");
                    fw3.close();

                }

                String NewFileName = fName.substring(0, fName.length() - 5);
                String Path = FileDir.getPath() + "\\" + NewFileName + ".html";
                //String=FileDir.getParent()+"\\"+mName+"";
                String replaceString = Path.replace('\\', '/');
                fw2.write("<tr><td>" + count + "</td><td><a  href=" + replaceString + " target=\"_blank\">" + fName + "</td><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + mName + "</td></tr>");
            }
            fw2.write("<tr><td>" + "Grand Total:" + "</td><td>" + "" + "</td><td>" + countPass + "</td><td>" + countFail + "</td></tr>");
            fw2.write("</table></body></html>");
            fw2.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
        System.out.println("Success File...");

        System.out.println("Total Number of TestCases Executed : " + count);
        //watch.stop();
        //long result = watch.getTime();
        //System.out.println("Total Execution Time : " + result+" ");


//        try {
//            int totalPass = new TestAutomation().countPass;
//            int totalFail = new TestAutomation().countFail;
//
//            FileWriter fw = new FileWriter(FileDir.getPath() + "\\Test Summary Report.htm");
//            fw.write("<html><head>\n" +
//                    "  <title>Bootstrap Example</title>\n" +
//
//                    "  <meta charset=\"utf-8\">\n" +
//                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
//                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
//                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
//                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
//
//                    "</head><body>");
//            fw.write("<h3>Test Summary Report</h3>");
//            fw.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3 \"><th>Pass </th><th>Fail </th><th>Total </th><th>Date & Time </th><td>URL</td><td>Username</td><td>Version</td></tr><tr><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + count + "</td><td>" + formatter.format(date) + "</td><td>" + RestAssured.baseURI + "</td><td>" + Username + "</td><td>" + Version + "</td></tr></table>");
//            fw.write("</body></html>");
//            fw.close();
//        } catch (Exception e) {
//            System.out.println(e);
//        }
        System.out.println("Success...");

    }

    public static void FullReportGenerate_OLD(String FileNameAllScenario) throws IOException, ParseException {

        File[] testScenarioDir = new File(System.getProperty("user.dir"), ReportGeneratePath).listFiles();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat dirformat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = new Date();
        String DirectoryPath = "C:\\APIAutomationSummaryReport_New";
        String foldername = DirectoryPath + "\\" + dirformat.format(date);
        File FileDir = new File(foldername);
        try {
            FileDir.mkdir();

        } catch (SecurityException e) {

        }

        //Prepare Test Scenario List

        JsonArray jsonArrayALLScenario = jsonScenarioFileNameList(PATIENT_ALLTEST_SCENARIO, "testscenario");

//        for (int i = 0; i < jsonArrayALLScenario.size(); i++) {
//            JsonObject jsonTestScenarioFileName = jsonArrayALLScenario.get(i).getAsJsonObject();
//            String FileScenarioList = jsonTestScenarioFileName.get("file").getAsString();
//
//            jsonArrayResponse(FileScenarioList, "testCases");
//
//            System.out.println("Value Report Scenario " + FileScenarioList);
//        }

        for (int i = 0; i < testScenarioDir.length; i++) {
            File directory = testScenarioDir[i];
            if (directory.isDirectory()) {
//                if (directory.getName().equalsIgnoreCase(FolderName)) {
                File[] files = directory.listFiles();
                for (int j = 0; j < files.length; j++) {
//                    System.out.println("File Name " + files[j].getName());
//                    jsonArrayResponse(files[j].toString(), "testCases");
                }
//                }
            }
        }

        System.out.println("\n\nFinal File List: " + scenariosFileSet);
        StopWatch watch = new StopWatch();


        //Execute Test Scenario List
        Iterator<File> files = scenariosFileSet.iterator();

        //String URL = new RestAssured().DEFAULT_URI;
        //String Username = new RefreshTokenScheduler().getRefreshToken().Username;
//        String URL = RestAssured.baseURI;
        List<NameValuePair> form = new ArrayList<>();
//        String Username = new RefreshTokenScheduler().UserName;
//        String Version = new RefreshTokenScheduler().verSion;
        try {
            //java.util.Date date= new java.util.Date();
            //Timestamp  a=new Timestamp(date.getTime());

            //String temp=a.toString();
            //System.out.println(temp);
            //File f1=new File(temp);
            //System.out.println(f1.mkdir());

            String Checkfilenamepath = FileDir.getPath();
            FileWriter fw2 = new FileWriter(Checkfilenamepath + "\\UI Testcase Wise Summary Report.htm");

            fw2.write("<h3>UI Testcase Wise Summary Report</h3>");
            fw2.write("<html><head>\n" +
                    "  <title>Bootstrap Example</title>\n" +
                    "  <meta charset=\"utf-8\">\n" +
                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
                    "</head><body>");
            fw2.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\"><th>Sr.No</th><th>File Name</th><th>Pass</th><th>Fail</th><th>Module Name</th></tr>");
            while (files.hasNext()) {

                File file = files.next();
                System.out.println("Value here " + file.toString());
                File fAllScenario = new File(FileNameAllScenario);
                System.out.println("Value Other " + fAllScenario.getName());

                System.out.println("\n\nExecuting Test Case: " + file.toString() + "\n");
                System.out.println("\n\nFile Name: " + file.getName() + "\n");

                System.out.println("\n\nFile Name: " + file.getParent().substring(file.getParent().lastIndexOf('\\') + 1) + "\n");

                String fName = file.getName().toString();
                String mName = file.getParent().substring(file.getParent().lastIndexOf('\\') + 1);

                int totalPass = 0;
                int totalFail = 0;

                if (file.getName().equalsIgnoreCase(fAllScenario.getName())) {

                    ++count;
//                //watch.start();
//                // call to the methods you want to benchmark

                    int oldtotalPass = countPass;
                    int oldtotalFail = countFail;
                    System.out.println("File Value Here " + file.toString());
                    runEntityCode(file.toString(), "");
                    System.out.println("Counter Pass " + countPass);
                    System.out.println("Counter Failed " + countFail);
//                int totalPass = new TestAutomation().countPass;
//                int totalFail = new TestAutomation().countFail;
//                if (oldtotalPass < totalPass) {
//
//                    totalPass = 1;
//                    totalFail = 0;
//
//                } else {
//                    totalPass = 0;
//                    totalFail = 1;

                    if (countPass < countFail) {
                        totalPass = 1;
                        totalFail = 0;
                    } else if (countPass == 0 && countFail == 0) {
                        totalPass = 1;
                        totalFail = 0;
                    } else {
                        totalPass = 0;
                        totalFail = 1;

                        int Length = fName.length() - 5;

                        String NewFileName = fName.substring(0, Length);
//
                        int Usindex = fName.indexOf("_");
                        String Id = NewFileName;
                        //String Md=NewModuleName;
                        //String Module=NewModuleName;
                        if (Usindex == -1) {

                        } else {
                            Id = fName.substring(0, Usindex);
                            //Md=mName.toString();
                        }

                        System.out.println(formatter.format(date));
                        System.out.println(LocalDateTime.now());
                        FileWriter fw3 = new FileWriter(FileDir.getPath() + "\\" + NewFileName + ".html");
                        //FileWriter fw3=new FileWriter(FileDir.getPath()+"\\"+NewModuleName+"");
                        fw3.write(" <h3>Fail Testcase Summary Report</h3>");

                        fw3.write("<html><head>\n" +
                                "  <title>Bootstrap Example</title>\n" +
                                "  <meta charset=\"utf-8\">\n" +
                                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
                                "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                                "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +

                                "</head><body>");

                        fw3.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\" ><th>TestCase_ID</th><th>TestCase_Name</th><th>Date</th></tr>" +
                                "<tr><td>" + Id + "</td><td>" + NewFileName + "</td><td>" + formatter.format(date) + "</td></tr>" +
                                "<tr><th colspan='2'>Expected Result:</th><td>" + 5 + "</td></tr>" +
                                "<tr><th colspan='2'>Actual Result:</th><td>" + 4 + "</td></tr>" +
                                "</table>");
                        fw3.write("</body></html>");
                        fw3.close();

                    }
                }

                String NewFileName = fName.substring(0, fName.length() - 5);
                String Path = FileDir.getPath() + "\\" + NewFileName + ".html";
                //String=FileDir.getParent()+"\\"+mName+"";
                String replaceString = Path.replace('\\', '/');
                fw2.write("<tr><td>" + count + "</td><td><a  href=" + replaceString + " target=\"_blank\">" + fName + "</td><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + mName + "</td></tr>");
            }
            fw2.write("<tr><td>" + "Grand Total:" + "</td><td>" + "" + "</td><td>" + countPass + "</td><td>" + countFail + "</td></tr>");
            fw2.write("</table></body></html>");
            fw2.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
//        System.out.println("Success File...");
//
//        System.out.println("Total Number of TestCases Executed : " + count);
//        //watch.stop();
//        //long result = watch.getTime();
//        //System.out.println("Total Execution Time : " + result+" ");
//
//
//        try {
//            int totalPass = new TestAutomation().countPass;
//            int totalFail = new TestAutomation().countFail;
//
//            FileWriter fw = new FileWriter(FileDir.getPath() + "\\Test Summary Report.htm");
//            fw.write("<html><head>\n" +
//                    "  <title>Bootstrap Example</title>\n" +
//
//                    "  <meta charset=\"utf-8\">\n" +
//                    "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
//                    "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
//                    "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
//                    "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
//
//                    "</head><body>");
//            fw.write("<h3>Test Summary Report</h3>");
//            fw.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3 \"><th>Pass </th><th>Fail </th><th>Total </th><th>Date & Time </th><td>URL</td><td>Username</td><td>Version</td></tr><tr><td>" + totalPass + "</td><td>" + totalFail + "</td><td>" + count + "</td><td>" + formatter.format(date) + "</td><td>" + RestAssured.baseURI + "</td><td>" + Username + "</td><td>" + Version + "</td></tr></table>");
//            fw.write("</body></html>");
//            fw.close();
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//        System.out.println("Success...");


    }

    public static JsonArray jsonScenarioFileNameList(String FileName, String keyValue) throws IOException, ParseException {
        //JSON parser object to parse read file
        JsonParser jsonScenario = new JsonParser();
        Object objectScenario = jsonScenario.parse(new FileReader(FileName));

        //convert Object to JSONObject
        JsonObject jsonTestScenario = (JsonObject) objectScenario;
        JsonArray jsonArrayTestScenario = (JsonArray) jsonTestScenario.get(keyValue);
        return jsonArrayTestScenario;
    }

    private static JSONArray jsonArrayResponse(String FileName, String keyValue, String FileNameAllScenario) throws IOException, ParseException {
        //JSON parser object to parse read file
        JSONParser jsonScenario = new JSONParser();
        Object objectScenario = jsonScenario.parse(new FileReader(FileName));

        //convert Object to JSONObject
        JSONObject jsonTestScenario = (JSONObject) objectScenario;
        JSONArray jsonArrayTestScenario = (JSONArray) jsonTestScenario.get(keyValue);

        if (jsonArrayTestScenario != null) {
            if (jsonArrayTestScenario.size() > 0) {
                for (int k = 0; k < jsonArrayTestScenario.size(); k++) {
                    JSONObject jsonFileName = (JSONObject) jsonArrayTestScenario.get(k);
                    String FileNameTestCases = (String) jsonFileName.get("file");
                    System.out.println("File name Test Execuated   " + FileNameTestCases);
                    File fAllScenario = new File(FileNameAllScenario);
                    System.out.println("File name Test Data   " + FileNameTestCases);
                    scenariosFileSet.add(new File(FileNameTestCases));
                }
            }
        }

        return jsonArrayTestScenario;
    }

}
