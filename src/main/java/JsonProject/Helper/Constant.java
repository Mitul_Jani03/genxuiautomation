package JsonProject.Helper;

import java.util.ArrayList;

public class Constant {

    private static final String RESOURCES = "resources/";
    public static String ReportGeneratePath = RESOURCES + "/testscenarios";

    public static String ValidationMSG = "This field is required.";

    // Report Generate Variable
    public static int countPass = 0;
    public static int countFail = 0;

}
