package JsonProject.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {

    String getChromePath = "C:\\Genx\\chromedriver.exe";
    String getFireFoxPath = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
    String getEdgePath = "C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe";
    public static Properties prop;
    //  public static WebDriver driver;

    public static WebDriver driver;

    @BeforeTest
    public void loadConfig() {
        try {
            prop = new Properties();
//            System.out.println("Super constructor invoked ");
            FileInputStream ip = new FileInputStream(
                    System.getProperty("user.dir") + "\\Configuration\\config.properties");
            prop.load(ip);
            System.out.println("driver: " + driver);

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public WebDriver launchApp() {
        String browserName = prop.getProperty("browser");

        if (browserName.equalsIgnoreCase("Chrome")) {
//            System.setProperty("webdriver.gecko.driver", this.getChromePath);


//            System.setProperty("webdriver.chrome.driver", this.getChromePath);
//            driver = new ChromeDriver();
//            driver.manage().window().maximize();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-notifications");
//            System.setProperty("webdriver.gecko.driver", this.getChromePath);
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(options);

        } else if (browserName.equalsIgnoreCase("FireFox")) {
            System.setProperty("Firefox Driver", this.getFireFoxPath);
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browserName.equalsIgnoreCase("Edge")) {
            System.setProperty("webdriver.edge.driver", this.getEdgePath);
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }

                                                                            driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //driver.get(prop.getProperty("url"));

        return driver;
    }
}